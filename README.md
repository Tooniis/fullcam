# FullCam
A free and open-source camera application for Android with the goal of including everything anyone would need from one.

<h3>Current Features:</h3>

*  Image and video capture
*  Full manual controls (focus and exposure)
*  Separate auto modes for focus (focus distance) and exposure  (sensor sensitivity and exposure time)
*  Resolution selection
*  Flash
*  Rule of thirds grid

FullCam is written in Kotlin and utilises Android's `Camera2` API.
