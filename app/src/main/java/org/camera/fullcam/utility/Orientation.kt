package org.camera.fullcam.utility

import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import android.view.OrientationEventListener
import java.lang.IllegalArgumentException

abstract class Orientation(context: Context): OrientationEventListener(context) {

    var threshold = 60
        set(value) {
            if(value in 1..89) field = value
            else throw IllegalArgumentException("Value must be in range 1..89")
        }
    var lastOrientation = 0
    var currentOrientation = lastOrientation
        private set(value) {
            lastOrientation = field
            field = value
            onOrientationChanged()
        }
    var changeClockwise = true

    // Orientation handler and its thread
    private val orientationThread = HandlerThread("BackgroundThread").apply { start() }
    private val orientationHandler = Handler(orientationThread.looper)

    // Orientation change of 1 degree
    override fun onOrientationChanged(orientation: Int) {
        try {
            orientationHandler.post {
                when (currentOrientation) {
                    // Orientation Changed by threshold in either direction
                    0 -> {
                        if (orientation in 0 + threshold..179) {
                            currentOrientation = 90
                            changeClockwise = true
                        } else if (orientation in 180..360 - threshold) {
                            currentOrientation = 270
                            changeClockwise = false
                        }
                    }
                    90 -> {
                        if (orientation > 90 + threshold) {
                            currentOrientation = 180
                            changeClockwise = true
                        } else if (orientation < 90 - threshold) {
                            currentOrientation = 0
                            changeClockwise = false
                        }
                    }
                    180 -> {
                        if (orientation > 180 + threshold) {
                            currentOrientation = 270
                            changeClockwise = true
                        } else if (orientation < 180 - threshold) {
                            currentOrientation = 90
                            changeClockwise = false
                        }
                    }
                    270 -> {
                        if (orientation > 270 + threshold) {
                            currentOrientation = 0
                            changeClockwise = true
                        } else if (orientation < 270 - threshold) {
                            currentOrientation = 180
                            changeClockwise = false
                        }
                    }
                }
            }
        } catch(e: IllegalStateException) {}
    }

    abstract fun onOrientationChanged()
}