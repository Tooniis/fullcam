package org.camera.fullcam.utility

import kotlin.math.round

fun pixels(dp: Int, density: Float): Int {
    return round(dp * density).toInt()
}
