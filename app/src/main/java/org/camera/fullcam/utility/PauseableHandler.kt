package org.camera.fullcam.utility

import android.os.Handler
import android.os.Looper
import android.os.Message
import java.util.*

class PauseableHandler(looper: Looper): Handler(looper) {
    private var stack: Stack<Message> = Stack()
    var active = false
        @Synchronized
        set(value) {
            field = value
            if(value) {
                while(!stack.empty()) {
                    sendMessageAtFrontOfQueue(stack.pop())
                }
            }
        }

    @Synchronized
    override fun dispatchMessage(msg: Message?) {
        if(!active)
        {
            stack.push(Message.obtain(msg))
            return
        }
        else {
            super.dispatchMessage(msg)
        }
    }

    @Synchronized
    fun clear() {
        stack = Stack()
    }
}