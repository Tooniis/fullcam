package org.camera.fullcam.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.view.View
import android.view.ViewGroup.LayoutParams
import androidx.core.content.ContextCompat

@SuppressLint("ViewConstructor")
class Grid(context: Context, width: Int, height: Int): View(context) {

    // Paint object from grid lines
    private val linePaint = Paint().apply {
        // Color (white)
        color = ContextCompat.getColor(context, android.R.color.white)

        // Stroke width (1px)
        strokeWidth = 0F
    }

    init {
        // Set dimensions
        layoutParams = LayoutParams(width, height)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        /* Rule Of Thirds */
        // Vertical lines
        canvas?.drawLine(width/3F, 0F, width/3F, height.toFloat(), linePaint)
        canvas?.drawLine(2*width/3F, 0F, 2*width/3F, height.toFloat(), linePaint)
        // Horizontal lines
        canvas?.drawLine(0F, height/3F, width.toFloat(), height/3F, linePaint)
        canvas?.drawLine(0F, 2*height/3F, width.toFloat(), 2*height/3F, linePaint)
    }
}