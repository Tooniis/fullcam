package org.camera.fullcam

import android.graphics.Point
import android.os.Handler
import android.util.Log
import android.hardware.camera2.*
import android.hardware.camera2.params.MeteringRectangle
import android.hardware.camera2.params.OutputConfiguration
import android.hardware.camera2.params.SessionConfiguration
import android.media.MediaActionSound
import android.os.HandlerThread
import android.util.Size
import android.view.Surface
import org.camera.fullcam.utility.PauseableHandler
import java.util.concurrent.Executor

private const val TAG = "Camera"

class Camera(private var cameraManager: CameraManager,
                      private var cameraId: String
) {
    abstract class FocusCallback {
        abstract fun onFocusChanged(locked: Boolean)
    }

    // Main objects
    private var camera: CameraDevice? = null
    private var captureSession: CameraCaptureSession? = null
    var sound: MediaActionSound? = null

    // Camera characteristics
    private var cameraCharacteristics: CameraCharacteristics
    var captureFormats: IntArray = IntArray(0)
    var imageCaptureSizes: MutableList<Array<Size>> = mutableListOf()
    var sensorSize: Size? = null
    var exposureTimeRange: LongRange? = null
    var sensorSensitivityRange: IntRange? = null
    var focusDistanceMinimum = 0F
    private var focusDistanceCalibration =
        CameraCharacteristics.LENS_INFO_FOCUS_DISTANCE_CALIBRATION_UNCALIBRATED

    // Threads and handlers
    private val backgroundThread = HandlerThread("BackgroundThread").apply { start() }
    private val backgroundHandler = Handler(backgroundThread.looper)
    private val cameraThread = HandlerThread("CameraThread").apply { start() }
    private val cameraHandler = PauseableHandler(cameraThread.looper)
    private val sessionThread = HandlerThread("SessionThread").apply { start() }
    private val sessionHandler = PauseableHandler(sessionThread.looper)

    // Surfaces
    var previewSurface: Surface? = null
        set(value) {
            // Restart capture session
            cameraHandler.post {
                if(captureSession != null) startSession()
                if(value != null) startPreview(value)
            }

            field = value
        }
    var imageSurface: Surface? = null
        set(value) {
            // Restart capture session
            cameraHandler.post {
                if(captureSession != null) startSession()
                previewSurface?.let { startPreview(it) }
            }

            field = value
        }
    var videoSurface: Surface? = null
        set(value) {
            // Restart capture session
            cameraHandler.post {
                if(captureSession != null) startSession()
                previewSurface?.let { startPreview(it) }
            }

            field = value
        }

    // Capture builders | TODO: Reduce code repetition
    private var previewBuilder: CaptureRequest.Builder? = null
        get() {

            val template =
                if(videoSurface == null) CameraDevice.TEMPLATE_PREVIEW
                else CameraDevice.TEMPLATE_RECORD

            Log.d(TAG, "Template: $template")

            camera?.let {
                field = it.createCaptureRequest(
                    template
                ).apply {

                    // Exposure mode
                    set(CaptureRequest.CONTROL_AE_MODE, exposureMode)
                    // Exposure modifiers (effective when AE is OFF)
                    set(CaptureRequest.SENSOR_SENSITIVITY, sensorSensitivity)
                    set(
                        CaptureRequest.SENSOR_EXPOSURE_TIME,
                        // Limit minimum exposure time to 1/10
                        if(exposureTime <= 100000000) exposureTime
                        else 100000000
                    )

                    // Focus Mode
                    set(CaptureRequest.CONTROL_AF_MODE, focusMode)
                    // Focus distance (effective when AF is OFF)
                    set(CaptureRequest.LENS_FOCUS_DISTANCE, focusDistance)
                    // Trigger auto focus if not continuous TODO: remove this
                    if(focusMode == CaptureRequest.CONTROL_AF_MODE_AUTO) {
                        // Set auto focus regions if any
                        if(focusRegionArray.isNotEmpty()) {
                            set(CaptureRequest.CONTROL_AF_REGIONS, focusRegionArray)
                        }
                        // Trigger focus
                        set(CaptureRequest.CONTROL_AF_TRIGGER,
                            CaptureRequest.CONTROL_AF_TRIGGER_START)
                    }

                    // Flash Mode
                    set(
                        CaptureRequest.FLASH_MODE,
                        if(flashMode == CaptureRequest.FLASH_MODE_TORCH) flashMode
                        else CaptureRequest.FLASH_MODE_OFF
                    )
                }
            }
            return field
        }
    private var captureBuilder: CaptureRequest.Builder? = null
        get() {
            camera?.let {
                field = it.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE).apply {

                    // Exposure mode
                    set(CaptureRequest.CONTROL_AE_MODE, exposureMode)
                    // Exposure modifiers (effective when AE is OFF)
                    set(CaptureRequest.SENSOR_EXPOSURE_TIME, exposureTime)
                    set(CaptureRequest.SENSOR_SENSITIVITY, sensorSensitivity)
                    // Trigger precapture metering when AE is ON
                    if(exposureMode == CaptureRequest.CONTROL_AE_MODE_ON) {
                        set(
                            CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                            CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START
                        )
                    }

                    // Focus Mode
                    set(CaptureRequest.CONTROL_AF_MODE, focusMode)
                    // Focus distance (effective when AF is OFF)
                    set(CaptureRequest.LENS_FOCUS_DISTANCE, focusDistance)
                    // Trigger auto focus if not continuous
                    if(focusMode == CaptureRequest.CONTROL_AF_MODE_AUTO) {
                        // Set auto focus regions if any
                        if(focusRegionArray.isNotEmpty()) {
                            set(CaptureRequest.CONTROL_AF_REGIONS, focusRegionArray)
                        }
                        // Trigger focus
                        set(CaptureRequest.CONTROL_AF_TRIGGER,
                            CaptureRequest.CONTROL_AF_TRIGGER_START)
                    }

                    // Flash Mode
                    set(CaptureRequest.FLASH_MODE, flashMode)
                }
            }
            return field
        }

    var frameDuration = 0L
    // Preview capture callback
    private var previewCallback =
        object: CameraCaptureSession.CaptureCallback() {
            override fun onCaptureCompleted(
                session: CameraCaptureSession,
                request: CaptureRequest,
                result: TotalCaptureResult
            ) {
                result.get(CaptureResult.CONTROL_AF_STATE)?.let { focusState = it }
                result.get(CaptureResult.SENSOR_FRAME_DURATION)?.let { frameDuration = it }
            }
    }

    // Focus
    var focusMode = CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE
        set(value) {
            field = value

            // Refresh preview to apply change
            previewSurface?.let { startPreview(it) }
        }
    var focusDistance: Float = 0F
        set(value) {
            field = value

            // Refresh preview if AF is OFF
            if(focusMode == CaptureRequest.CONTROL_AF_MODE_OFF) {
                previewSurface?.let { startPreview(it) }
            }
        }
    private var focusRegionArray = arrayOf<MeteringRectangle>()
    private var focusRegionSize = Size(64, 64)
    private var focusRegionWeight = 1
    private var focusState = CaptureResult.CONTROL_AF_STATE_INACTIVE
        set(value) {
            // Call FocusCallback.onFocusChanged if state has changed
            if(value != field) {
                focusCallback?.apply {
                    when (value) {
                        CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED -> {
                            onFocusChanged(true)
                            sound?.play(MediaActionSound.FOCUS_COMPLETE)
                        }
                        CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED -> {
                            onFocusChanged(false)
                        }
                    }
                }
            }

            field = value
        }
    private var focusCallback: FocusCallback? = null

    // Exposure
    var exposureMode = CaptureRequest.CONTROL_AE_MODE_ON
        set(value) {
            field = value
            // Refresh preview to apply change
            previewSurface?.let { startPreview(it) }
        }
    var exposureTime: Long = 0
        set(value) {
            field = value

            // Refresh preview if AE is OFF
            if(exposureMode == CaptureRequest.CONTROL_AE_MODE_OFF) {
                previewSurface?.let { startPreview(it) }
            }
        }
    var sensorSensitivity = 400
        set(value) {
            field = value

            // Refresh preview is AE is OFF
            if(exposureMode == CaptureRequest.CONTROL_AE_MODE_OFF) {
                previewSurface?.let { startPreview(it) }
            }
        }

    // Flash
    var flashMode = CaptureRequest.FLASH_MODE_OFF


    init {
        cameraManager.getCameraCharacteristics(cameraId).let {
            cameraCharacteristics = it
            it.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)?.let { map ->
                captureFormats = map.outputFormats
                for (format in captureFormats) {
                    imageCaptureSizes.add(map.getOutputSizes(format))
                }
            }

            // Get needed camera characteristics (constants, ranges, ...)
            // Sensor size (constant)
            it.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE)?.let { rect ->
                sensorSize = Size(rect.width(), rect.height())
            }
            // Exposure time (range)
            it.get(CameraCharacteristics.SENSOR_INFO_EXPOSURE_TIME_RANGE)?.let { range ->
                exposureTimeRange = range.lower..range.upper
            }
            // Sensor sensitivity (range)
            it.get(CameraCharacteristics.SENSOR_INFO_SENSITIVITY_RANGE)?.let { range ->
                sensorSensitivityRange = (range.lower)..(range.upper)
            }
            // Minimum focus distance (constant)
            it.get(CameraCharacteristics.LENS_INFO_MINIMUM_FOCUS_DISTANCE)?.let{ distance ->
                focusDistanceMinimum = distance
            }
            // Focus distance calibration (constant)
            it.get(CameraCharacteristics.LENS_INFO_FOCUS_DISTANCE_CALIBRATION)?.let {calibration ->
                focusDistanceCalibration = calibration
            }

            val fpsTarget = it.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES)
            fpsTarget?.let { fps ->
                for (i in fps) {
                    Log.d(TAG, "FPS: $i")
                }
            }
        }
    }

    fun initCamera() {
        captureSession?.let {
            sessionHandler.active = false
            it.close()
        }
        camera?.let {
            cameraHandler.active = false
            it.close()
            camera = null
        }
        try {
            cameraManager.openCamera(cameraId, cameraStateCallback, backgroundHandler)
        } catch (e: SecurityException) {
            Log.e(TAG, "Permission required to open camera")
        }
    }

    fun closeCamera() {
        // Stop session
        stopSession()

        camera?.apply {
            // Deactivate camera handler
            cameraHandler.active = false

            // Close camera
            close()
            camera = null
        }
    }

    private val cameraStateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(cameraDevice: CameraDevice) {
            Log.d(TAG, "Camera opened")
            camera = cameraDevice
            cameraHandler.active = true
        }

        override fun onDisconnected(cameraDevice: CameraDevice) {
            Log.e(TAG, "Camera disconnected")
            camera = null
        }

        override fun onError(cameraDevice: CameraDevice, error: Int) {
            Log.e(TAG, "Error opening camera: $error")
            camera = null
        }
    }

    private val sessionStateCallback = object : CameraCaptureSession.StateCallback() {
        override fun onConfigureFailed(session: CameraCaptureSession) {
            Log.e(TAG, "Session configuration failed")
            captureSession = null
        }

        override fun onConfigured(session: CameraCaptureSession) {
            Log.d(TAG, "Session Configured")
            captureSession = session
            sessionHandler.active = true
        }
    }

    fun startSession() {
        sessionHandler.active = false
        sessionHandler.clear()
        cameraHandler.post {
            // Stop existing capture session
            captureSession?.apply {
                abortCaptures()
                close()
            }

            // Make a list of output configurations
            val outputConfigurations: MutableList<OutputConfiguration> = mutableListOf()
            previewSurface?.let { outputConfigurations.add(OutputConfiguration(it)) }
            imageSurface?.let { if(videoSurface == null) outputConfigurations.add(OutputConfiguration(it)) }
            videoSurface?.let { outputConfigurations.add(OutputConfiguration(it)) }

            // Get a new session configuration
            val sessionConfiguration = SessionConfiguration(
                SessionConfiguration.SESSION_REGULAR,
                outputConfigurations,
                Executor { command -> cameraHandler.post(command) },
                sessionStateCallback
            )

            // Start the capture session
            camera?.createCaptureSession(sessionConfiguration)
        }
    }

    fun stopSession() {
        captureSession?.let {
            // Deactivate session handler
            sessionHandler.active = false

            // Close session
            it.close()
        }
    }

    private fun startPreview(surface: Surface) {
        sessionHandler.post {
            camera?.let {
                captureSession?.setRepeatingRequest(
                    previewBuilder!!.apply {
                        addTarget(surface)
                        videoSurface?.let {
                            addTarget(it)
                        }
                    }.build(),
                    previewCallback,
                    sessionHandler
                )

                if (captureSession == null) {
                    Log.e(TAG, "Session is null. Cannot start preview.")
                }
            }
        }
    }

    fun focus(x: Int, y: Int, callback: FocusCallback?) {
        // Get focus callback
        focusCallback = callback

        // Set region array
        focusRegionArray = arrayOf(
            MeteringRectangle(
                Point(x - focusRegionSize.width / 2, y - focusRegionSize.height / 2),
                focusRegionSize,
                focusRegionWeight
            )
        )

        // Refresh preview session to apply
        previewSurface?.let { startPreview(it) }
    }

    fun captureImage() {
        val captureCallback = object : CameraCaptureSession.CaptureCallback() {
            override fun onCaptureCompleted(
                session: CameraCaptureSession,
                request: CaptureRequest,
                result: TotalCaptureResult
            ) {
                Log.d(TAG, "Image captured")
                sound?.play(MediaActionSound.SHUTTER_CLICK)
            }
        }

        sessionHandler.post {
            camera?.let {
                imageSurface?.let { surface ->
                    captureSession?.capture(
                        captureBuilder?.apply {
                            addTarget(surface)
                        }!!.build(),
                        captureCallback,
                        sessionHandler
                    )
                }
            }
        }
    }
}