package org.camera.fullcam

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color.*
import android.graphics.ImageFormat
import android.graphics.Typeface
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CaptureRequest
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.View
import android.view.animation.RotateAnimation
import android.view.animation.ScaleAnimation
import android.widget.Button
import android.widget.LinearLayout
import android.widget.SeekBar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import kotlinx.android.synthetic.main.activity_fullscreen.*
import org.camera.fullcam.utility.Orientation
import org.camera.fullcam.utility.pixels
import org.camera.fullcam.view.Grid
import kotlin.math.*

private const val TAG = "Activity"
private const val FOCUS_DISTANCE = 1
private const val SENSOR_SENSITIVITY = 2
private const val EXPOSURE_TIME = 3
private const val MODE_IMAGE = 0
private const val MODE_VIDEO = 1

@SuppressLint("SetTextI18n")
class Activity:
    AppCompatActivity(),
    View.OnTouchListener,
    View.OnClickListener,
    SeekBar.OnSeekBarChangeListener {

    private val handler = Handler(HandlerThread("Thread").apply { start() }.looper)

    // User Interface
    private lateinit var orientation: Orientation
    private val displayMetrics = DisplayMetrics()
    private lateinit var vibrator: Vibrator
    private var grid: Grid? = null
    private var buttonList: List<View> = listOf()
    private var resolutionButtonList: List<View> = listOf()
    private var colorAccent: Int? = null
        set(value) {
            field = value

            // Apply change to all UI elements
            value?.let {
                setupAdjustment.thumb.setTint(it)
            }
        }
    private var lastSettingElement = -1
    private var currentSettingElement = -1
        set(value) {
            // Remember the last setting element
            lastSettingElement = field

            field = value

            when(value) {
                -1 -> {
                    // Set Colors
                    focusDistanceButton.background.setTint(getColor(R.color.interfaceColor))
                    sensorSensitivityButton.background.setTint(getColor(R.color.interfaceColor))
                    exposureTimeButton.background.setTint(getColor(R.color.interfaceColor))

                    // Disable setupAdjustment and set limits
                    setupAdjustment.min = 0
                    setupAdjustment.max = 1
                    setupAdjustment.isEnabled = false
                }
                FOCUS_DISTANCE -> {
                    // Set Colors
                    focusDistanceButton.background.setTint(colorAccent!!)
                    sensorSensitivityButton.background.setTint(getColor(R.color.interfaceColor))
                    exposureTimeButton.background.setTint(getColor(R.color.interfaceColor))

                    // Enable setupAdjustment and set limits
                    setupAdjustment.min = 0
                    setupAdjustment.max = (mainCamera.focusDistanceMinimum * 10).toInt()
                    setupAdjustment.isEnabled = true
                }
                SENSOR_SENSITIVITY -> {
                    // Set Colors
                    focusDistanceButton.background.setTint(getColor(R.color.interfaceColor))
                    sensorSensitivityButton.background.setTint(colorAccent!!)
                    exposureTimeButton.background.setTint(getColor(R.color.interfaceColor))

                    // Enable setupAdjustment and set limits
                    mainCamera.sensorSensitivityRange?.let {
                        setupAdjustment.min = log(it.first / 100.0, 2.0).roundToInt()
                        setupAdjustment.max = log(it.last / 100.0, 2.0).roundToInt()
                        setupAdjustment.isEnabled = true
                    }
                }
                EXPOSURE_TIME -> {
                    // Set Colors
                    focusDistanceButton.background.setTint(getColor(R.color.interfaceColor))
                    sensorSensitivityButton.background.setTint(getColor(R.color.interfaceColor))
                    exposureTimeButton.background.setTint(colorAccent!!)

                    // Enable setupAdjustment and set limits
                    mainCamera.exposureTimeRange?.let {
                        Log.d(TAG, "Exposure Time")
                        setupAdjustment.min = (log(it.first / 1000000000.0, 2.0)).roundToInt()
                        setupAdjustment.max = (log(it.last / 1000000000.0, 2.0)).roundToInt()
                        setupAdjustment.progress = exposureTimeStep
                        Log.d(TAG, "${setupAdjustment.progress}")
                        Log.d(TAG, "${setupAdjustment.min} ${setupAdjustment.max}")
                        Log.d(TAG, it.toString())
                        setupAdjustment.isEnabled = true
                    }
                }
            }
        }

    // Camera
    private lateinit var mainCamera: Camera
    private lateinit var cameraId: String
    private lateinit var cameraManager: CameraManager

    // Output
    private lateinit var image: Image
    private lateinit var video: Video

    // Setting
    private var mode = MODE_IMAGE
    private var captureFormat = ImageFormat.JPEG
    private var captureSize = Size(1,1)
        set(size) {
            field = size
            resolutionButton.text = "${size.width}\n${size.height}"
            viewport.layoutParams.height = (displayMetrics.widthPixels * captureAspectRatio)
                .toInt()
            Log.d(TAG, "Capture: " +
                    "${captureSize.width} ${captureSize.height} " +
                    "$captureAspectRatio" + "\n" +
                    "Viewport: " +
                    "${viewport.layoutParams.width} ${viewport.layoutParams.height}"
            )
        }
    private var captureAspectRatio: Float = 1F
        get() {
            field = captureSize.width.toFloat() / captureSize.height
            return field
        }

    private var flashMode = false
        set(value) {
            field = value
            if(value) flashButton.setImageResource(R.drawable.ic_flash_on)
            else flashButton.setImageResource(R.drawable.ic_flash_off)
        }
    private var focusDistance = 0F
        set(value) {
            field = value
            mainCamera.focusDistance = value
            when(mainCamera.focusDistance) {
                0F -> focusDistanceValue.text = "∞"
                else -> focusDistanceValue.text = mainCamera.focusDistance.toString()
            }
        }
    private var sensorSensitivityStep = 0
        set(value) {
            field = value
            mainCamera.sensorSensitivity = (2.0.pow(value)*100).toInt()
            sensorSensitivityValue.text = mainCamera.sensorSensitivity.toString()
        }
    private var exposureTimeStep = -1
        set(value) {
            field = value
            mainCamera.exposureTime = (2.0.pow(value)*1000000000).toLong()
            exposureTimeValue.text =
                if(value >= 0)
                    (2.0.pow(value)).toInt().toString()
                else
                    "1/${(2.0.pow(abs(value))).toInt()}"
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                View.SYSTEM_UI_FLAG_FULLSCREEN or
        View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY    // hide the status bar
        supportActionBar?.hide()    // hide the title bar
        setContentView(R.layout.activity_fullscreen)

        // Get buttons
        buttonList = listOf(
            // Environment buttons
            modeButton,
            flashButton,
            gridButton,
            resolutionButton,
            focusModeButton,
            exposureModeButton,
            // Capture setting buttons
            focusDistanceButton,
            sensorSensitivityButton,
            exposureTimeButton
        )

        // Get Colors
        colorAccent = argb(
            128,
            getColor(R.color.colorAccent).red,
            getColor(R.color.colorAccent).green,
            getColor(R.color.colorAccent).blue
        )

        // Check and fix permissions
        getPermission(this, Manifest.permission.CAMERA)
        getPermission(this, Manifest.permission.RECORD_AUDIO)
        getPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        // Get display metrics
        this.windowManager.defaultDisplay.getMetrics(displayMetrics)

        // Set controlBar height
        controlBar.layoutParams.height = displayMetrics.heightPixels / 4

        // Initialize vibrator
        vibrator = this.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        // Initialize the camera
        cameraManager = this.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        cameraId = cameraManager.cameraIdList[0]
        mainCamera = Camera(cameraManager, cameraId)
        // mainCamera.sound = MediaActionSound()
        mainCamera.initCamera()
        captureSize =
            mainCamera.imageCaptureSizes[mainCamera.captureFormats.indexOf(captureFormat)][0]

        // Initialize outputs
        image = Image(captureSize, captureFormat)
        video = Video()

        // Set listeners and callbacks
        viewport.holder.addCallback(viewportHolderCallback)
        for(button in buttonList) {
            button.setOnClickListener(this)
        }
        setupAdjustment.setOnSeekBarChangeListener(this)

        // Disable buttons if required
        if(mainCamera.focusMode != CaptureRequest.CONTROL_AF_MODE_OFF) {
                focusDistanceButton.isEnabled = false
        }
        if(mainCamera.exposureMode == CaptureRequest.CONTROL_AE_MODE_ON) {
            exposureTimeButton.isEnabled = false
            sensorSensitivityButton.isEnabled = false
        }

        // Start listening for orientation changes
        orientation = object: Orientation(this) {
            override fun onOrientationChanged() {
                // Calculate reverse rotation
                var lastRotation = 360-orientation.lastOrientation.toFloat()
                var currentRotation = 360-orientation.currentOrientation.toFloat()
                // Ensure rotation takes the shortest way around
                if(abs(lastRotation - currentRotation) > 90F) {
                    if(lastRotation == 0F || lastRotation == 360F) {
                        lastRotation = 360 - lastRotation
                    }
                    else if(currentRotation == 0F || currentRotation == 360F) {
                        currentRotation = 360 - currentRotation
                    }
                }

                // Rotate buttons
                for(button in buttonList+resolutionButtonList) {
                    // Initialize animation
                    val animation = RotateAnimation(
                        lastRotation,
                        currentRotation,
                        button.width / 2F,
                        button.height / 2F
                    ).apply {
                        duration = 500
                        // Retain rotation after animation
                        isFillEnabled = true
                        fillAfter = true
                    }
                    button.startAnimation(animation)
                }
            }
        }.apply { enable() }
    }

    @SuppressLint("SetTextI18n")
    private fun initResolutionList(sizeArray: Array<Size>, layout: LinearLayout): List<Button> {
        val buttons: MutableList<Button> = mutableListOf()
        for(index in sizeArray.indices) {
            Button(layout.context).let {
                it.setBackgroundColor(alpha(0))

                it.text = "${sizeArray[index].width}\n${sizeArray[index].height}"
                it.typeface = Typeface.create("sans-serif-thin", Typeface.NORMAL)
                it.textSize = 12F
                it.setTextColor(rgb(1F, 1F, 1F))

                layout.addView(it)
                it.layoutParams.width = pixels(48, displayMetrics.density)
                buttons.add(it)
            }
        }
        return buttons
    }

    override fun onPause() {
        super.onPause()

        Log.d(TAG, "Paused")
        mainCamera.apply {
            stopSession()
            closeCamera()
        }
    }
    /*
    override fun onResume() {
        Log.d(TAG, "Resumed")
        mainCamera.apply {
            initCamera()
            previewSurfaceIndex?.let { startPreview(it) }
        }
        super.onResume()
    }
    */
    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        view.performClick()

        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                when (view) {
                    shutterButton -> {
                        when(mode) {
                            MODE_IMAGE -> {
                                // Capture an image
                                mainCamera.captureImage()

                                // Set button image
                                shutterButton.setImageResource(R.drawable.ic_shutter_capture)

                            }
                        }
                        vibrator.vibrate(
                            VibrationEffect.createOneShot(10, 255)
                        )    // Give haptic feedback
                    }
                    viewport -> {
                        // Start focus sequence if focus mode is auto
                        if (mainCamera.focusMode == CaptureRequest.CONTROL_AF_MODE_AUTO) {
                            // Set position
                            focusTarget.x = motionEvent.x - focusTarget.width / 2
                            focusTarget.y = motionEvent.y - focusTarget.height / 2

                            // Initialize animation
                            val animation = ScaleAnimation(
                                0F, 1F,
                                0F, 1F,
                                motionEvent.x,
                                motionEvent.y
                            ).apply { duration = 200 }

                            // Set color
                            focusTarget.setColorFilter(getColor(R.color.lightText))

                            // Make visible
                            focusTarget.visibility = View.VISIBLE

                            // Animate
                            focusTarget.startAnimation(animation)

                            // Focus
                            mainCamera.sensorSize?.let { size ->
                                mainCamera.focus(
                                    round(size.width / viewport.height.toFloat()
                                            * motionEvent.y).toInt(),
                                    size.height
                                            - round(size.height / viewport.width.toFloat()
                                            * motionEvent.x).toInt(),
                                    object: Camera.FocusCallback() {
                                        override fun onFocusChanged(locked: Boolean) {
                                            focusTarget.setColorFilter(
                                                if(locked) getColor(R.color.colorGreen)
                                                else getColor(R.color.colorRed)
                                            )

                                            // Make invisible after delay
                                            handler.postDelayed({
                                                focusTarget.visibility = View.INVISIBLE
                                            }, 2000
                                            )
                                        }
                                    }
                                )
                            }
                        }
                    }
                }
            }
            MotionEvent.ACTION_UP ->
                when(view) {
                    shutterButton -> {
                        when(mode) {
                            MODE_IMAGE -> {
                                // Set button image
                                shutterButton.setImageResource(R.drawable.ic_shutter_image)
                            }
                            MODE_VIDEO -> {
                                if(!video.recording) {
                                    // Start recording video
                                    video.record()

                                    // Set button image
                                    shutterButton.setImageResource(R.drawable.ic_shutter_recording)
                                }
                                else {
                                    // Stop recording video
                                    video.stop()

                                    // Set button image
                                    shutterButton.setImageResource(R.drawable.ic_shutter_video)
                                }
                            }
                        }
                    }
                }
        }
        return true
    }

    override fun onClick(view: View) {
        when(view) {
            // Environment Setting Buttons
            modeButton -> {
                when(mode) {
                    MODE_IMAGE -> {
                        // Switch to video mode
                        mode = MODE_VIDEO

                        // Set video surface
                        mainCamera.videoSurface = video.surface

                        // Initiate FPS counter
                        val runnable = object: Runnable {
                            override fun run() {
                                Log.d("FPS: ", "${
                                1 / (mainCamera.frameDuration.toDouble() / 1000000000)
                                        }")
                                handler.postDelayed({ run() }, 500)
                            }
                        }
                        handler.post(runnable)

                        // Change button images
                        modeButton.setImageResource(R.drawable.ic_video)
                        shutterButton.setImageResource(R.drawable.ic_shutter_video)
                    }
                    MODE_VIDEO -> {
                        // Switch to image mode
                        mode = MODE_IMAGE

                        // Remove video surface
                        mainCamera.videoSurface = null

                        // Change button images
                        modeButton.setImageResource(R.drawable.ic_still_image)
                        shutterButton.setImageResource(R.drawable.ic_shutter_image)
                    }
                }
            }
            flashButton -> {
                // Toggle flash mode
                flashMode = !flashMode
                mainCamera.flashMode =
                    if(flashMode) CaptureRequest.FLASH_MODE_SINGLE
                    else CaptureRequest.FLASH_MODE_OFF
            }
            gridButton -> {
                grid?.apply {
                    when(visibility) {
                        View.VISIBLE -> {
                            // Toggle grid visibility
                            visibility = View.GONE

                            // Change grid button image
                            gridButton.setImageResource(R.drawable.ic_grid_disabled)

                        }
                        View.GONE -> {
                            // Toggle grid visibility
                            visibility = View.VISIBLE

                            // Change grid button image
                            gridButton.setImageResource(R.drawable.ic_grid)
                        }
                    }
                }
            }
            resolutionButton -> {
                resolutionList.let {
                    // Toggle visibility
                    when (it.visibility) {
                        View.VISIBLE -> it.visibility = View.GONE
                        View.GONE -> it.visibility = View.VISIBLE
                    }
                }
            }
            focusModeButton -> {
                when(mainCamera.focusMode) {
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE -> {
                        // Switch to continuous
                        mainCamera.focusMode = CaptureRequest.CONTROL_AF_MODE_AUTO
                        focusModeButton.setImageResource(R.drawable.ic_focus_mode_auto)
                    }
                    CaptureRequest.CONTROL_AF_MODE_AUTO-> {
                        // Switch to manual
                        mainCamera.focusMode = CaptureRequest.CONTROL_AF_MODE_OFF

                        // Set manual control element
                        if(lastSettingElement == FOCUS_DISTANCE) {
                            currentSettingElement = lastSettingElement
                        }

                        // Enable manual control button
                        focusDistanceButton.isEnabled = true
                        // Change mode button image
                        focusModeButton.setImageResource(R.drawable.ic_focus_mode_manual)
                        // Change value color
                        focusDistanceValue.setTextColor(getColor(R.color.lightText))
                        // Change value string by calling setter
                        focusDistance = focusDistance
                    }
                    CaptureRequest.CONTROL_AF_MODE_OFF -> {
                        // Switch to auto
                        mainCamera.focusMode = CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE

                        // Set manual control element
                        if(currentSettingElement == FOCUS_DISTANCE) {
                            currentSettingElement = -1
                        }

                        // Disable manual control button
                        focusDistanceButton.isEnabled = false
                        // Change mode button image
                        focusModeButton.setImageResource(R.drawable.ic_focus_mode_continuous)
                        // Change value color
                        focusDistanceValue.setTextColor(getColor(R.color.auto))
                        // Change value string
                        focusDistanceValue.text = getString(R.string.null_value)
                    }
                }
            }
            exposureModeButton -> {
                when(mainCamera.exposureMode) {
                    CaptureRequest.CONTROL_AE_MODE_ON -> {
                        // Switch to manual
                        mainCamera.exposureMode = CaptureRequest.CONTROL_AE_MODE_OFF

                        // Set manual control element
                        when(lastSettingElement) {
                            EXPOSURE_TIME, SENSOR_SENSITIVITY -> {
                                currentSettingElement = lastSettingElement
                            }
                        }

                        // Enable manual control buttons
                        exposureTimeButton.isEnabled = true
                        sensorSensitivityButton.isEnabled = true
                        // Change mode button image
                        exposureModeButton.setImageResource(R.drawable.ic_exposure_mode_manual)
                        // Change value colors
                        exposureTimeValue.setTextColor(getColor(R.color.lightText))
                        sensorSensitivityValue.setTextColor(getColor(R.color.lightText))
                        // Change value strings by calling setters
                        exposureTimeStep = exposureTimeStep
                        sensorSensitivityStep = sensorSensitivityStep
                    }
                    CaptureRequest.CONTROL_AE_MODE_OFF -> {
                        // Switch to auto
                        mainCamera.exposureMode = CaptureRequest.CONTROL_AE_MODE_ON

                        // Set manual control element
                        when(currentSettingElement) {
                            EXPOSURE_TIME, SENSOR_SENSITIVITY -> currentSettingElement = -1
                        }

                        // Disable manual control buttons
                        exposureTimeButton.isEnabled = false
                        sensorSensitivityButton.isEnabled = false
                        // Change mode button image
                        exposureModeButton.setImageResource(R.drawable.ic_exposure_mode_auto)
                        // Change value text colors
                        exposureTimeValue.setTextColor(getColor(R.color.auto))
                        sensorSensitivityValue.setTextColor(getColor(R.color.auto))
                        // Change value text strings
                        exposureTimeValue.text = getString(R.string.null_value)
                        sensorSensitivityValue.text = getString(R.string.null_value)
                    }
                }
            }

            // Capture Setting Buttons
            focusDistanceButton -> {
                currentSettingElement = FOCUS_DISTANCE
            }
            sensorSensitivityButton -> {
                currentSettingElement = SENSOR_SENSITIVITY
            }
            exposureTimeButton -> {
                currentSettingElement = EXPOSURE_TIME
            }
        }
    }

    // Seekbar events
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        vibrator.vibrate(VibrationEffect.createOneShot(1, 50))
        when(currentSettingElement) {
            EXPOSURE_TIME -> {
                exposureTimeStep = progress
            }
            FOCUS_DISTANCE -> {
                if (progress <= mainCamera.focusDistanceMinimum * 10) {
                    focusDistance = progress.toFloat() / 10
                }
            }
            SENSOR_SENSITIVITY -> {
                sensorSensitivityStep = progress
            }
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }


    private fun getPermission(activity: Activity, permission: String) {
        // Check if permission is not granted
        if (ContextCompat.checkSelfPermission(activity, permission)
            != PackageManager.PERMISSION_GRANTED) {
            // Ask for permission
            ActivityCompat.requestPermissions(activity, arrayOf(permission), 0)
        }
    }

    private val viewportHolderCallback = object: SurfaceHolder.Callback {
        override fun surfaceChanged(surfaceHolder: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {}
        override fun surfaceDestroyed(surfaceHolder: SurfaceHolder?) {}
        override fun surfaceCreated(surfaceHolder: SurfaceHolder?) {
            surfaceHolder?.let { holder ->
                // Set preview surface view size
                holder.setFixedSize(
                    (1080 * captureAspectRatio).toInt(),
                    1080
                )

                // Create grid
                // Make a new grid
                grid = Grid(this@Activity, 1080, (1080 * captureAspectRatio).toInt()).apply {
                    // Disable by default
                    visibility = View.GONE
                }
                // Add grid to view
                mainFrame.addView(grid)

                // Add output surfaces to camera
                mainCamera.imageSurface = image.surface

                // Start session and preview
                mainCamera.previewSurface = holder.surface
                mainCamera.startSession()

                // Activate shutter button
                shutterButton.setOnTouchListener(this@Activity)

                // Activate touch-to-focus
                viewport.setOnTouchListener(this@Activity)

                // Initialize resolution list
                resolutionButtonList =
                    initResolutionList(
                        mainCamera.
                            imageCaptureSizes[mainCamera.captureFormats.indexOf(captureFormat)],
                        resolutionListLayout
                    )
                handler.post {
                    // Set resolution buttons actions
                    for (index in resolutionButtonList.indices) {
                        resolutionButtonList[index].setOnClickListener {
                            // Set capture size
                            captureSize =
                                mainCamera.
                                    imageCaptureSizes[mainCamera.
                                        captureFormats.indexOf(captureFormat)][index]

                            // Set preview surface size
                            captureSize.let {
                                holder.setFixedSize(
                                    (1080 * captureAspectRatio).toInt(),
                                    1080
                                )
                                image.size = it
                            }

                            // Refresh session and preview
                            mainCamera.previewSurface = mainCamera.previewSurface

                            // Hide resolution list
                            resolutionList.visibility = View.GONE

                            // Refresh grid
                            // Remove old grid
                            mainFrame.removeView(grid)
                            // Make a new grid with the new dimensions
                            grid = Grid(
                                this@Activity,
                                1080, (1080 * captureAspectRatio).toInt()
                            ).apply {
                                // Retain old visibility
                                grid?.let { visibility = it.visibility }
                            }
                            // Add new grid to view
                            mainFrame.addView(grid)
                        }
                    }
                }
            }
        }
    }
}