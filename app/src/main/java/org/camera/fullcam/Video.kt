package org.camera.fullcam

import android.media.MediaRecorder
import android.os.Environment
import android.util.Log
import android.util.Size
import android.view.Surface
import java.io.File
import java.lang.IllegalStateException
import java.util.*

private const val TAG = "Video"

class Video {

    // Main objects
    private val recorder = MediaRecorder()
    var surface: Surface

    // Output file
    private val calendar = Calendar.getInstance()
    private var outputFileName = "VID.jpeg"
        get() {
            field = "VID-" +
                    "${calendar.get(Calendar.SECOND)}-" +
                    "${calendar.get(Calendar.MINUTE)}-" +
                    "${calendar.get(Calendar.HOUR)}--"  +
                    "${calendar.get(Calendar.DATE)}-"   +
                    "${calendar.get(Calendar.MONTH)}-"  +
                    "${calendar.get(Calendar.YEAR)}"    +
                    ".mp4"

            return field
        }
    private var outputFile = File(
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
        outputFileName
    )
        get() {
            field = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                outputFileName
            )
            return field
        }

    // Object state
    var recording = false
        private set

    // Parameters
    var format = MediaRecorder.OutputFormat.MPEG_4
    // Video
    var videoEncoder = MediaRecorder.VideoEncoder.DEFAULT
    var videoBitrate = 50000000
    var videoSize = Size(1920, 1080)
    // Audio
    var audioSource = MediaRecorder.AudioSource.CAMCORDER
    var audioChannels = 2
    var audioEncoder = MediaRecorder.AudioEncoder.DEFAULT
    var audioBitrate = 192000

    init {
        // Prepare recorder
        prepare()

        // Obtain recorder surface
        surface = recorder.surface
    }

    private fun prepare() {
        recorder.apply {
            // General parameters
            setVideoSource(MediaRecorder.VideoSource.SURFACE)
            try { setAudioSource(audioSource) }
            catch (e: IllegalStateException) {
                Log.e(TAG, "Cannot set audio source. Check permissions.")
            }
            setOutputFormat(format)

            // Video parameters
            setVideoEncoder(videoEncoder)
            setVideoSize(videoSize.width, videoSize.height)
            setVideoEncodingBitRate(videoBitrate)

            // Audio parameters
            setAudioChannels(audioChannels)
            setAudioEncoder(audioEncoder)
            setAudioEncodingBitRate(audioBitrate)

            setOutputFile(outputFile)
            prepare()
        }
    }

    fun record() {
        recorder.start()
        recording = true

        Log.d(TAG, "Recording started")
    }

    fun stop() {
        recorder.stop()
        recording = false

        Log.d(TAG, "Recording stopped")
    }
}