package org.camera.fullcam

import android.media.Image
import android.media.ImageReader
import android.os.Environment
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Size
import android.view.Surface
import java.io.File
import java.io.IOException
import java.util.*

private const val TAG = "Image"

class Image(initSize: Size, initFormat: Int) {

    // Main objects
    lateinit var surface: Surface
    private var image: Image? = null
    private var imageReader: ImageReader

    // Calendar
    private val calendar = Calendar.getInstance()

    // Image properties
    var size: Size = initSize
        set(value) {
            imageReader = initReader(value, format)
        }
    var format: Int = initFormat
        set(value) {
            imageReader = initReader(size, value)
        }

    // Image handler and its thread
    private var imageThread = HandlerThread("ImageThread").apply { start() }
    private var imageHandler = Handler(imageThread.looper)

    // Image availability listener
    private val listener = ImageReader.OnImageAvailableListener { reader ->
        Log.d(TAG, "Image available")

        // Acquire image from reader
        reader?.let {
            image = it.acquireNextImage()
            Log.d(TAG, "Image acquired")
        }

        // Initialize output file object
        val outputFile = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
            outputFileName
        )

        image?.let {
            // Convert image to byte array
            val buffer = it.planes[0].buffer
            val byteArray = ByteArray(buffer.remaining())
            buffer.get(byteArray)

            try {
                // Write byte array to output file
                outputFile.writeBytes(byteArray)
                Log.d(TAG, "Image saved as ${outputFile.name}")
            } catch(e: IOException) {
                Log.e(TAG, "Cannot save image")
            }
            it.close()
        }
    }

    private var outputFileName = "IMG.jpeg"
        get() {
            field = "IMG-" +
                    "${calendar.get(Calendar.SECOND)}-" +
                    "${calendar.get(Calendar.MINUTE)}-" +
                    "${calendar.get(Calendar.HOUR)}--"  +
                    "${calendar.get(Calendar.DATE)}-"   +
                    "${calendar.get(Calendar.MONTH)}-"  +
                    "${calendar.get(Calendar.YEAR)}"    +
                    ".jpeg"

            return field
        }

    init {
        // Initiate reader
        imageReader = initReader(size, format)
    }

    private fun initReader(readerSize: Size, readerFormat: Int): ImageReader {
        ImageReader.newInstance(readerSize.width, readerSize.height, readerFormat, 1).let {
            // Obtain surface from reader
            surface = it.surface
            // Set image availability listener
            it.setOnImageAvailableListener(listener, imageHandler)

            return it
        }
    }
}